Name:           automotive-image-builder
Version:        0.5.0
Release:        1%{?dist}
Summary:        A tool to build (and run) automotive images

License:        MIT
URL:            https://gitlab.com/CentOS/automotive/src/automotive-image-builder
Source0:        automotive-image-builder-%{version}.tar.gz

BuildArch:	noarch
BuildRequires:  make
Requires:       android-tools
Requires:       osbuild
Requires:       osbuild-auto
Requires:       osbuild-luks2
Requires:       osbuild-lvm2
Requires:       osbuild-ostree
Requires:       ostree
Requires:       python3-jsonschema
Requires:       python3-pyyaml

%description
Tool to build (and run) automotive images

%prep
%autosetup

%build


%install
%make_install

%files
%license LICENSE
%doc README.md
%{_bindir}/automotive-image-builder
%{_bindir}/automotive-image-runner
%{_bindir}/automotive-image-vm
%{_prefix}/lib/automotive-image-builder

%changelog
* Fri Mar  7 2025 Alexander Larsson <alexl@redhat.com> - 0.5.0-1
- Update to 0.5.0

* Fri Feb 28 2025 Mark Kemel <mkemel@redhat.com> - 0.4.1-1
- Update to 0.4.1

* Fri Feb 7 2025 Alexander Larsson <alexl@redhat.com> - 0.4.0-1
- Update to 0.4.0

* Mon Dec 16 2024 Alexander Larsson <alexl@redhat.com> - 0.3.0-1
- Update to 0.3.0

* Tue Nov 26 2024 Alexander Larsson <alexl@redhat.com> - 0.2.2-1
- Add python3-jsonschema dependency
- Update to 0.2.2

* Tue Nov 26 2024 Alexander Larsson <alexl@redhat.com> - 0.2.1-1
- Update to 0.2.1

* Fri Oct 25 2024 Alexander Larsson <alexl@redhat.com> - 0.1.8-1
- Update to 0.1.8

* Fri Aug 30 2024 Alexander Larsson <alexl@redhat.com> - 0.1.7-1
- Update to 0.1.7

* Wed Jul 10 2024 Alexander Larsson <alexl@redhat.com> - 0.1.6-1
- Update to 0.1.6
- Added python3-pyyaml dependency


* Fri Jun 28 2024 Alexander Larsson <alexl@redhat.com> - 0.1.5-1
- Update to 0.1.5

* Wed May 15 2024 Alexander Larsson <alexl@redhat.com> - 0.1.4-1
- Update to 0.1.4

* Mon May 13 2024 Alexander Larsson <alexl@redhat.com> - 0.1.3-1
- Update to 0.1.3


* Fri May 10 2024 Alexander Larsson <alexl@redhat.com> - 0.1.2-1
- Update to 0.1.2

* Tue Apr 30 2024 Alexander Larsson <alexl@redhat.com> - 0.1.1-1
- Update to 0.1.1

* Fri Apr 26 2024 Alexander Larsson <alexl@redhat.com>
- Initial version
